package com.GFormation.core.model;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import java.lang.Override;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="groups")
public class Group implements Serializable
{


   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private long id ;
   //@Version
   @Column(name = "version")
   private int version = 0;

   public Group() {
   }
   public Group(String title) {
		super();
		this.title = title;
   }
   
   @Column(length = 20)
   private String title;
   
   @OneToMany
   @JoinColumn(name="group_id")
   private Set<User> users = new HashSet<User>();

   @ManyToMany(cascade=CascadeType.ALL , fetch=FetchType.EAGER)
	   @JoinTable(name="group_roles",  
	   joinColumns={@JoinColumn(name="group_id", referencedColumnName="id")},  
	   inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")}) 
   private Set<Role> roles = new HashSet<Role>();
   
   public Set<User> getUsers() {
	   return users;
   }
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
//============================================================================ 
   public long getId()
   {
      return this.id;
   }

   public void setId(final int id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object that)
   {
      if (this == that)
      {
         return true;
      }
      if (that == null)
      {
         return false;
      }
      if (getClass() != that.getClass())
      {
         return false;
      }

      return super.equals(that);
   }



   public String getTitle()
   {
      return this.title;
   }

   public void setTitle(final String title)
   {
      this.title = title;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (title != null && !title.trim().isEmpty())
         result += "title: " + title;
      return result;
   }
}
