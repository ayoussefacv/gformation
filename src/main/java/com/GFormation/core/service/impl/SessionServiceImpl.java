package com.GFormation.core.service.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.GFormation.core.dao.DAO;
import com.GFormation.core.dao.SessionCollaboratorDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.SessionCollaborator;
import com.GFormation.core.model.User;
import com.GFormation.core.service.SessionService;

@Transactional
public class SessionServiceImpl implements SessionService{
	
	@Autowired
	UserDao userDao;
	@Autowired
	SessionDao sessionDao;
	@Autowired
	SessionCollaboratorDao sessionCollaboratorDao;
	
	@Override
	public boolean addCollaboratorToSession(long collaboratorId, long sessionId) {

		User collaborator = userDao.getById(collaboratorId);
			System.out.println("collaborator="+collaborator);
		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);
			
		SessionCollaborator	sessionCollaborator =new SessionCollaborator();
		sessionCollaborator.setSession(session);
		sessionCollaborator.setCollaborator(collaborator);
		System.out.println("session.getCollaborators()="+session.getCollaborators().toString());
		
		session.getSessionCollaborators().add(sessionCollaborator);
		//sessionCollaboratorDao.add(sessionCollaborator);
		sessionDao.update(session);
		return true;
	}

	@Override
	@Transactional
	public void removeCollaborator(int sessionId , int collaboratorId) {

		User collaborator = userDao.getById(collaboratorId);
			System.out.println("collaborator="+collaborator);
		Session session = sessionDao.getById(sessionId);
			System.out.println("session="+session);

			SessionCollaborator	sessionCollaborator =new SessionCollaborator();
			sessionCollaborator.setSession(session);
			sessionCollaborator.setCollaborator(collaborator);
		//session.echo();
		session.removeCollaborator(collaborator);
		//System.out.println("##### =====> \n session.toString()="+sessionCollaborator.getSession().getId());
		sessionCollaboratorDao.delete(sessionCollaborator);
		//sessionDao.add(session);
		//session.echo();	
	}



}