package com.GFormation.core.service;

import com.GFormation.core.model.Session;

public interface SessionService{

	boolean addCollaboratorToSession(long collaboratorId, long sessionId);

	void removeCollaborator(int sessionId, int collaboratorId);

}
