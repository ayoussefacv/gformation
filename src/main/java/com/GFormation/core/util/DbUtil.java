package com.GFormation.core.util;
 
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
 







import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;
import com.GFormation.core.model.User;
 
public class DbUtil {

	@Autowired
	private UserDao userDao;
	@Autowired
	private GroupDao groupDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private TrainingDao trainingDao;
	
	public void initialize(){
		if(groupDao.getById(1) == null)
		{
			System.out.println("####################### initialising DB :");
			Group group = new Group("ADMIN");
			group.getRoles().add(new Role("ROLE_ADMIN"));
			groupDao.add(group);
	
			User user=new User("admin","21232f297a57a5a743894a0e4a801fc3","admin@admin.com");
			user.setGroup(group);
			userDao.add(user);
			
			
			group = new Group("USER");
			group.getRoles().add( new Role("ROLE_USER"));
			groupDao.add(group);
			//roleDao.add(role);
			
			user=new User("user","ee11cbb19052e40b07aac0ca060c23ee","user@user.com");
			user.setGroup(group);
			userDao.add(user);


			Training training = new Training();
			training.setTitle("t1");
			training.setDescription("t1");
			
			Session session = new Session();
			session.setTitle("s1");
			session.setDescription("s1");
			
			training.getSessions().add(session);
			
			trainingDao.add(training); 
			
			
			System.out.println("####################### end initialising DB :");
		}
	
	}
}