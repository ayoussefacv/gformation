package com.GFormation.core.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.TrainingDao;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Training;
import com.GFormation.core.service.SessionService;


@Controller
@RequestMapping(value="RF/sessions")
public class RF_SessionController {
	private static final Logger logger =LoggerFactory.getLogger(RF_SessionController.class);

	@Autowired
	TrainingDao trainingDAO;
	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;
	@Autowired
	SessionService sessionService;

	//============================================================================================= show
	@RequestMapping("/{id}")
    public ModelAndView show(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Session session = this.sessionDAO.getById(id);
		if(session != null)
		{
			modelAndView.setViewName("RF/session/show");
			modelAndView.addObject("session", session);
	        return modelAndView;
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no_session_found_with_this_id");
        return modelAndView;
    }

	//============================================================================================= add
	
	//============================================================================================= update
	@RequestMapping("/{session_id}/update")
    public ModelAndView update(@PathVariable("session_id") int session_id){

		ModelAndView modelAndView = new ModelAndView("RF/session/update");
		modelAndView.addObject("session", this.sessionDAO.getById(session_id));
    	System.out.println(this.sessionDAO.getById(session_id));
        return modelAndView;
    }
	
    @RequestMapping(value={"/{session_id}/update"} , method = RequestMethod.POST)
    public ModelAndView update( @ModelAttribute("session") Session s){

        sessionDAO.update(s);
    	        ModelAndView modelAndView = new ModelAndView("redirect:/RF/sessions/"+s.getId());
        return modelAndView;
    }
	//============================================================================================= delete
    @RequestMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id){
        
    	long training_id =sessionDAO.getById(id).getTraining().getId();
    	//sessionDAO.deleteById(id);
    	System.out.println("eeeee");
    	Training training = trainingDAO.getById(training_id);
    	training.getSessions().remove(sessionDAO.getById(id)) ;//deleteById(id);
    	trainingDAO.update(training);
    	
    	return "redirect:/trainings/"+training_id;
    }
	//============================================================================================= remove collaborator
    @RequestMapping("/{sessionId}/collaborators/{collaboratorId}/delete")
    public String removeCollaborator(@PathVariable("sessionId") int sessionId,@PathVariable("collaboratorId") int collaboratorId){
        
    	sessionService.removeCollaborator(sessionId,collaboratorId);
    	
    	return "redirect:/RF/sessions/"+sessionId;
    }


    
    
    
}
