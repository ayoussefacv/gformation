package com.GFormation.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.User;


@Controller
@RequestMapping(value="/admin")
public class AdminController {
	private static final Logger logger =LoggerFactory.getLogger(AdminController.class);

	@Autowired
	UserDao userDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;


	
	@RequestMapping({"/admin"})
	public ModelAndView homeIndexPage(){
		
		logger.info("entering  /admin/DashBoard");
		return new ModelAndView("/admin/DashBoard");
	}
	
	@RequestMapping(value="/users_manager", method=RequestMethod.GET)
	public ModelAndView users_managerPage() {
		
		logger.info("users_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/users_manager");
		
		List<User> users = userDAO.getAll();
		modelAndView.addObject("users", users);
		return modelAndView;
	}
	
	@RequestMapping(value="/users_manager", method=RequestMethod.POST)
	public ModelAndView users_manager_add_userPage(@ModelAttribute("SpringWeb")User user, 
			   ModelMap model){
		
		
		ModelAndView modelAndView = new ModelAndView("admin/users_manager");
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		User newuser =new User(user.getUsername(),encodedPassword,user.getEmail(),groupDAO.getByTitle("USER"));
		userDAO.add(newuser);
		

		List<User> users = userDAO.getAll();
		modelAndView.addObject("users", users);
		return modelAndView;
	}
	
	@RequestMapping(value="/users_manager/add_user", method=RequestMethod.GET)
	public ModelAndView add_userPage() {
		
		logger.info("groups_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/add_user");
				
		return new ModelAndView("/admin/add_user", "command", new User());
	
	}
	
	@RequestMapping(value="/groups_manager", method=RequestMethod.GET)
	public ModelAndView groups_managerPage() {
		
		logger.info("groups_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/groups_manager");
		
		List<Group> groups = groupDAO.getAll();
		
		modelAndView.addObject("groups", groups);
		
		return modelAndView;
	
	}
	
	@RequestMapping(value="/roles_manager", method=RequestMethod.GET)
	public ModelAndView roles_managerPage() {
		
		logger.info("roles_managerPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("admin/roles_manager");
		
		List<Group> groups = groupDAO.getAll();
		List<Role> roles = roleDAO.getAll();
		
		modelAndView.addObject("roles", roles);
		modelAndView.addObject("groups_role", new Group());
		modelAndView.addObject("groups", groups);
		
		return modelAndView;
	
	}


}
