package com.GFormation.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.UserDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.User;
import com.GFormation.core.service.UserService;


@Controller
@RequestMapping(value="/profile")
public class ProfileController {
	private static final Logger logger =LoggerFactory.getLogger(ProfileController.class);

	@Autowired
	UserDao userdao;
	@Autowired
	GroupDao groupDao;
	
	@RequestMapping({"/show"})
	public ModelAndView homeIndexPage(){
		List<User> users = userdao.getAll();

		System.out.println(users.toString());
		userdao.delete(users.get(0));
		 users = userdao.getAll();
		System.out.println(users.toString());

		System.out.println(userdao.findUserByName("user").toString());
		System.out.println("entering  /profile/show");
		return new ModelAndView("/profile/show");
	}
	
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public ModelAndView addProjectPage() {
		
		logger.info("addProjectPage Method : GET");
		
		ModelAndView modelAndView = new ModelAndView("profile/update");
		
		User user =userdao.add(new User("username1","password1","email1",groupDao.getByTitle("USER")));
		System.out.println("user.getRoles()="+user.getGroup().getRoles().toString());

		modelAndView.addObject("user", user);
		return modelAndView;
	}


}
