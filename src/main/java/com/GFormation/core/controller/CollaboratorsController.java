package com.GFormation.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.GFormation.core.dao.GroupDao;
import com.GFormation.core.dao.RoleDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.dao.SessionDao;
import com.GFormation.core.model.Group;
import com.GFormation.core.model.Role;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.Session;
import com.GFormation.core.model.User;


@Controller
@RequestMapping(value="/sessions")
public class CollaboratorsController {
	
	private static final Logger logger =LoggerFactory.getLogger(AdminController.class);

	@Autowired
	SessionDao sessionDAO;
	@Autowired
	GroupDao groupDAO;
	@Autowired
	RoleDao roleDAO;


	//============================================================================================= list 
	@RequestMapping("/{id}/collaborators")
    public ModelAndView list(@PathVariable("id") int id){

		ModelAndView modelAndView = new ModelAndView();
		Session session = this.sessionDAO.getById(id);
		if(session != null)
		{
			modelAndView.setViewName("session/show");
			Set<User> collaborators = session.getCollaborators();
			modelAndView.addObject("session", session);
			modelAndView.addObject("collaborators", collaborators);
	        return modelAndView;
		}
		modelAndView.setViewName("404");
		modelAndView.addObject("message", "no_session_found_with_this_id");
        return modelAndView;
    }

}
